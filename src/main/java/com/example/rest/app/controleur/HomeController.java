package com.example.rest.app.controleur;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

    @GetMapping("/toto")
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok("Hello Toto");
    }
}
