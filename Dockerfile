FROM openjdk:13-alpine
RUN apk add --no-cache bash
COPY target/demo-0.0.1-SNAPSHOT.jar /app.jar
CMD ["java", "-jar", "/app.jar"]